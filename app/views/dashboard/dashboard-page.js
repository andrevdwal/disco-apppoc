var Observable = require("data/observable").Observable;

var dashboardData = new Observable({
    welcomeMessage: "sss"
})

exports.loaded = function(args) {
    page = args.object;
    page.bindingContext = dashboardData;
}

exports.onNavigatingTo = function(args) {
    dashboardData.welcomeMessage = "Hi " + args.object.navigationContext.username + ", welcome back!";
}

