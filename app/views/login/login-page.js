var Observable = require("data/observable").Observable;
var frameModule = require("ui/frame");

var user = new Observable({
    username: "andrev",
    password: "password"
})

exports.loaded = function(args) {
    page = args.object;
    page.bindingContext = user;
}

exports.signin = function() {
    var data = {
        moduleName: "views/dashboard/dashboard-page",
        context: {
            username: page.getViewById("username").text
        }
    }

    var topmost = frameModule.topmost();
    topmost.navigate(data);
}

exports.locateBranch = function() {
    alert("No branches nearby");
}

exports.moreInfo = function() {
    alert("Google is your friend...");
}